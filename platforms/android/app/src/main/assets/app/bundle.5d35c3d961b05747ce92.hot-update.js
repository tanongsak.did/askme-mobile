"use strict";
exports.id = "bundle";
exports.ids = null;
exports.modules = {

/***/ "./src/app/item/items.component.ts":
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ItemsComponent": () => (/* binding */ ItemsComponent)
/* harmony export */ });
/* harmony import */ var _item_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./src/app/item/item.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/@angular/core/fesm2015/core.mjs");
/* harmony import */ var _nativescript_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/@nativescript/angular/fesm2015/nativescript-angular.mjs");




class ItemsComponent {
    constructor(itemService) {
        this.itemService = itemService;
    }
    ngOnInit() {
        this.items = this.itemService.getItems();
    }
}
ItemsComponent.ɵfac = function ItemsComponent_Factory(t) { return new (t || ItemsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_item_service__WEBPACK_IMPORTED_MODULE_0__.ItemService)); };
ItemsComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ItemsComponent, selectors: [["ns-items"]], decls: 20, vars: 0, consts: [["title", "My App"], ["text", "\u0E2B\u0E31\u0E01\u0E19\u0E23\u0E1F\u0E2B\u0E01\u0E1F\u0E19\u0E23\u0E19\u0E23"], ["width", "210", "height", "210", "backgroundColor", "lightgray"], ["text", "10, 10", "left", "10", "top", "10", "width", "90", "height", "90", "backgroundColor", "red"], ["text", "110, 10", "left", "110", "top", "10", "width", "90", "height", "90", "backgroundColor", "green"], ["text", "110, 110", "left", "110", "top", "110", "width", "90", "height", "90", "backgroundColor", "blue"], ["text", "10, 110", "left", "10", "top", "110", "width", "90", "height", "90", "backgroundColor", "yellow"], ["columns", "60, auto, *", "rows", "60, auto, *", "width", "230", "height", "230", "backgroundColor", "lightgray"], ["text", "Label 1", "row", "0", "col", "0", "backgroundColor", "red"], ["text", "Label 2", "row", "0", "col", "1", "colSpan", "2", "backgroundColor", "green"], ["text", "Label 3", "row", "1", "col", "0", "rowSpan", "2", "backgroundColor", "blue"], ["text", "Label 4", "row", "1", "col", "1", "backgroundColor", "yellow"], ["text", "Label 5", "row", "1", "col", "2", "backgroundColor", "orange"], ["text", "Label 6", "row", "2", "col", "1", "backgroundColor", "pink"], ["text", "Label 7", "row", "2", "col", "2", "backgroundColor", "purple"]], template: function ItemsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ActionBar", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "template");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "sdsdsad");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "Label", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "AbsoluteLayout", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](8, "Label", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "Label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "Label", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](11, "Label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "GridLayout", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](13, "Label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "Label", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "Label", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "Label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "Label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "Label", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](19, "Label", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } }, directives: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_2__.ActionBarComponent], encapsulation: 2 });


/***/ })

};
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVuZGxlLjVkMzVjM2Q5NjFiMDU3NDdjZTkyLmhvdC11cGRhdGUuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBQTZDO0FBQ1Q7QUFDQztBQUNPO0FBQ3JDO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyREFBMkQsaUNBQWlDLCtEQUFvQixDQUFDLHNEQUFjO0FBQy9ILG9DQUFvQywrREFBb0IsR0FBRyx5M0NBQXkzQztBQUNwN0MsUUFBUSx1REFBWTtBQUNwQixRQUFRLDREQUFpQjtBQUN6QixRQUFRLG9EQUFTO0FBQ2pCLFFBQVEsNERBQWlCO0FBQ3pCLFFBQVEsb0RBQVM7QUFDakIsUUFBUSwwREFBZTtBQUN2QixRQUFRLG9EQUFTO0FBQ2pCLFFBQVEsMERBQWU7QUFDdkIsUUFBUSx1REFBWTtBQUNwQixRQUFRLDREQUFpQjtBQUN6QixRQUFRLHVEQUFZO0FBQ3BCLFFBQVEsdURBQVk7QUFDcEIsUUFBUSx1REFBWTtBQUNwQixRQUFRLHVEQUFZO0FBQ3BCLFFBQVEsMERBQWU7QUFDdkIsUUFBUSw0REFBaUI7QUFDekIsUUFBUSx1REFBWTtBQUNwQixRQUFRLHVEQUFZO0FBQ3BCLFFBQVEsdURBQVk7QUFDcEIsUUFBUSx1REFBWTtBQUNwQixRQUFRLHVEQUFZO0FBQ3BCLFFBQVEsdURBQVk7QUFDcEIsUUFBUSx1REFBWTtBQUNwQixRQUFRLDBEQUFlO0FBQ3ZCLE9BQU8sZUFBZSxxRUFBcUIscUJBQXFCIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vQXNrTWUvLi9zcmMvYXBwL2l0ZW0vaXRlbXMuY29tcG9uZW50LnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEl0ZW1TZXJ2aWNlIH0gZnJvbSAnLi9pdGVtLnNlcnZpY2UnO1xuaW1wb3J0ICogYXMgaTAgZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCAqIGFzIGkxIGZyb20gXCIuL2l0ZW0uc2VydmljZVwiO1xuaW1wb3J0ICogYXMgaTIgZnJvbSBcIkBuYXRpdmVzY3JpcHQvYW5ndWxhclwiO1xuZXhwb3J0IGNsYXNzIEl0ZW1zQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcihpdGVtU2VydmljZSkge1xuICAgICAgICB0aGlzLml0ZW1TZXJ2aWNlID0gaXRlbVNlcnZpY2U7XG4gICAgfVxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLml0ZW1zID0gdGhpcy5pdGVtU2VydmljZS5nZXRJdGVtcygpO1xuICAgIH1cbn1cbkl0ZW1zQ29tcG9uZW50Lsm1ZmFjID0gZnVuY3Rpb24gSXRlbXNDb21wb25lbnRfRmFjdG9yeSh0KSB7IHJldHVybiBuZXcgKHQgfHwgSXRlbXNDb21wb25lbnQpKGkwLsm1ybVkaXJlY3RpdmVJbmplY3QoaTEuSXRlbVNlcnZpY2UpKTsgfTtcbkl0ZW1zQ29tcG9uZW50Lsm1Y21wID0gLypAX19QVVJFX18qLyBpMC7Jtcm1ZGVmaW5lQ29tcG9uZW50KHsgdHlwZTogSXRlbXNDb21wb25lbnQsIHNlbGVjdG9yczogW1tcIm5zLWl0ZW1zXCJdXSwgZGVjbHM6IDIwLCB2YXJzOiAwLCBjb25zdHM6IFtbXCJ0aXRsZVwiLCBcIk15IEFwcFwiXSwgW1widGV4dFwiLCBcIlxcdTBFMkJcXHUwRTMxXFx1MEUwMVxcdTBFMTlcXHUwRTIzXFx1MEUxRlxcdTBFMkJcXHUwRTAxXFx1MEUxRlxcdTBFMTlcXHUwRTIzXFx1MEUxOVxcdTBFMjNcIl0sIFtcIndpZHRoXCIsIFwiMjEwXCIsIFwiaGVpZ2h0XCIsIFwiMjEwXCIsIFwiYmFja2dyb3VuZENvbG9yXCIsIFwibGlnaHRncmF5XCJdLCBbXCJ0ZXh0XCIsIFwiMTAsIDEwXCIsIFwibGVmdFwiLCBcIjEwXCIsIFwidG9wXCIsIFwiMTBcIiwgXCJ3aWR0aFwiLCBcIjkwXCIsIFwiaGVpZ2h0XCIsIFwiOTBcIiwgXCJiYWNrZ3JvdW5kQ29sb3JcIiwgXCJyZWRcIl0sIFtcInRleHRcIiwgXCIxMTAsIDEwXCIsIFwibGVmdFwiLCBcIjExMFwiLCBcInRvcFwiLCBcIjEwXCIsIFwid2lkdGhcIiwgXCI5MFwiLCBcImhlaWdodFwiLCBcIjkwXCIsIFwiYmFja2dyb3VuZENvbG9yXCIsIFwiZ3JlZW5cIl0sIFtcInRleHRcIiwgXCIxMTAsIDExMFwiLCBcImxlZnRcIiwgXCIxMTBcIiwgXCJ0b3BcIiwgXCIxMTBcIiwgXCJ3aWR0aFwiLCBcIjkwXCIsIFwiaGVpZ2h0XCIsIFwiOTBcIiwgXCJiYWNrZ3JvdW5kQ29sb3JcIiwgXCJibHVlXCJdLCBbXCJ0ZXh0XCIsIFwiMTAsIDExMFwiLCBcImxlZnRcIiwgXCIxMFwiLCBcInRvcFwiLCBcIjExMFwiLCBcIndpZHRoXCIsIFwiOTBcIiwgXCJoZWlnaHRcIiwgXCI5MFwiLCBcImJhY2tncm91bmRDb2xvclwiLCBcInllbGxvd1wiXSwgW1wiY29sdW1uc1wiLCBcIjYwLCBhdXRvLCAqXCIsIFwicm93c1wiLCBcIjYwLCBhdXRvLCAqXCIsIFwid2lkdGhcIiwgXCIyMzBcIiwgXCJoZWlnaHRcIiwgXCIyMzBcIiwgXCJiYWNrZ3JvdW5kQ29sb3JcIiwgXCJsaWdodGdyYXlcIl0sIFtcInRleHRcIiwgXCJMYWJlbCAxXCIsIFwicm93XCIsIFwiMFwiLCBcImNvbFwiLCBcIjBcIiwgXCJiYWNrZ3JvdW5kQ29sb3JcIiwgXCJyZWRcIl0sIFtcInRleHRcIiwgXCJMYWJlbCAyXCIsIFwicm93XCIsIFwiMFwiLCBcImNvbFwiLCBcIjFcIiwgXCJjb2xTcGFuXCIsIFwiMlwiLCBcImJhY2tncm91bmRDb2xvclwiLCBcImdyZWVuXCJdLCBbXCJ0ZXh0XCIsIFwiTGFiZWwgM1wiLCBcInJvd1wiLCBcIjFcIiwgXCJjb2xcIiwgXCIwXCIsIFwicm93U3BhblwiLCBcIjJcIiwgXCJiYWNrZ3JvdW5kQ29sb3JcIiwgXCJibHVlXCJdLCBbXCJ0ZXh0XCIsIFwiTGFiZWwgNFwiLCBcInJvd1wiLCBcIjFcIiwgXCJjb2xcIiwgXCIxXCIsIFwiYmFja2dyb3VuZENvbG9yXCIsIFwieWVsbG93XCJdLCBbXCJ0ZXh0XCIsIFwiTGFiZWwgNVwiLCBcInJvd1wiLCBcIjFcIiwgXCJjb2xcIiwgXCIyXCIsIFwiYmFja2dyb3VuZENvbG9yXCIsIFwib3JhbmdlXCJdLCBbXCJ0ZXh0XCIsIFwiTGFiZWwgNlwiLCBcInJvd1wiLCBcIjJcIiwgXCJjb2xcIiwgXCIxXCIsIFwiYmFja2dyb3VuZENvbG9yXCIsIFwicGlua1wiXSwgW1widGV4dFwiLCBcIkxhYmVsIDdcIiwgXCJyb3dcIiwgXCIyXCIsIFwiY29sXCIsIFwiMlwiLCBcImJhY2tncm91bmRDb2xvclwiLCBcInB1cnBsZVwiXV0sIHRlbXBsYXRlOiBmdW5jdGlvbiBJdGVtc0NvbXBvbmVudF9UZW1wbGF0ZShyZiwgY3R4KSB7IGlmIChyZiAmIDEpIHtcbiAgICAgICAgaTAuybXJtWVsZW1lbnQoMCwgXCJBY3Rpb25CYXJcIiwgMCk7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50U3RhcnQoMSwgXCJ0ZW1wbGF0ZVwiKTtcbiAgICAgICAgaTAuybXJtXRleHQoMiwgXCJcXG4gIFwiKTtcbiAgICAgICAgaTAuybXJtWVsZW1lbnRTdGFydCgzLCBcImFcIik7XG4gICAgICAgIGkwLsm1ybV0ZXh0KDQsIFwic2RzZHNhZFwiKTtcbiAgICAgICAgaTAuybXJtWVsZW1lbnRFbmQoKTtcbiAgICAgICAgaTAuybXJtXRleHQoNSwgXCJcXG5cIik7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50RW5kKCk7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50KDYsIFwiTGFiZWxcIiwgMSk7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50U3RhcnQoNywgXCJBYnNvbHV0ZUxheW91dFwiLCAyKTtcbiAgICAgICAgaTAuybXJtWVsZW1lbnQoOCwgXCJMYWJlbFwiLCAzKTtcbiAgICAgICAgaTAuybXJtWVsZW1lbnQoOSwgXCJMYWJlbFwiLCA0KTtcbiAgICAgICAgaTAuybXJtWVsZW1lbnQoMTAsIFwiTGFiZWxcIiwgNSk7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50KDExLCBcIkxhYmVsXCIsIDYpO1xuICAgICAgICBpMC7Jtcm1ZWxlbWVudEVuZCgpO1xuICAgICAgICBpMC7Jtcm1ZWxlbWVudFN0YXJ0KDEyLCBcIkdyaWRMYXlvdXRcIiwgNyk7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50KDEzLCBcIkxhYmVsXCIsIDgpO1xuICAgICAgICBpMC7Jtcm1ZWxlbWVudCgxNCwgXCJMYWJlbFwiLCA5KTtcbiAgICAgICAgaTAuybXJtWVsZW1lbnQoMTUsIFwiTGFiZWxcIiwgMTApO1xuICAgICAgICBpMC7Jtcm1ZWxlbWVudCgxNiwgXCJMYWJlbFwiLCAxMSk7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50KDE3LCBcIkxhYmVsXCIsIDEyKTtcbiAgICAgICAgaTAuybXJtWVsZW1lbnQoMTgsIFwiTGFiZWxcIiwgMTMpO1xuICAgICAgICBpMC7Jtcm1ZWxlbWVudCgxOSwgXCJMYWJlbFwiLCAxNCk7XG4gICAgICAgIGkwLsm1ybVlbGVtZW50RW5kKCk7XG4gICAgfSB9LCBkaXJlY3RpdmVzOiBbaTIuQWN0aW9uQmFyQ29tcG9uZW50XSwgZW5jYXBzdWxhdGlvbjogMiB9KTtcbiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==